# Standard library imports.
import argparse
import json
import sys
from datetime import datetime
from datetime import timezone
from urllib.parse import urlparse

# requests package imports.
import requests


# The default number of documents that will be written in bulk for one
# HTTP request.
NUM_DOCS = 1000

# Define the UTC timezone.
UTC = timezone.utc


def _gen_doc_bak(orig_doc):

    doc = {}

    for key in orig_doc.keys():
        if key not in ('_rev', '_attachments',):
            doc[key] = orig_doc[key]

    # Handle the attachments from the original document.  Remember that
    # if the actual data is not in the attachment fields, then the
    # attachments will be deleted.
    for key, value in orig_doc.get('_attachments', {}).items():

        if 'data' in value.keys():

            if '_attachments' not in doc.keys():
                doc['_attachments'] = {}

            if key not in doc['_attachments'].keys():
                doc['_attachments'][key] = {}

            for k in ('data', 'content_type',):
                doc['_attachments'][key][k] = value[k]

    # Add the backup string to the end of the document ID.
    doc['_id'] += '.bak-' + datetime.now(UTC).strftime('%Y%m%d%H%M%S.%f')
    #doc['_id'] += bak_dt

    return doc


def _get_docs_dict(docs):
    """Return a dictionary of documents by document ID.

    """
    if _is_docs(docs) == False:
        return {}

    # Populate this dictionary with valid documents, mapped to the
    # documents' ID values.
    docs_dict = {}

    for doc in docs['docs']:
        if _is_doc(doc) == True:
            docs_dict[doc['_id']] = doc

    return docs_dict


def _get_docs_list(docs):
    """Return the list of documents from the given dictionary.

    Given a dictionary containing a list of CouchDB documents as the
    value of the "docs" key, return the list of valid documents.  A
    valid document contains a non-empty string value for the "_id"
    key.

    """
    if _is_docs(docs) == False:
        return []

    # Populate this list with valid documents.
    docs_list = []

    for doc in docs['docs']:
        if _is_doc(doc) == True:
            docs_list.append(doc)

    return docs_list


def _get_token(url, **kwargs):
    # Check if there is a user name and password in the arguments.  If
    # so, retrieve a session token.  If a session token is provided,
    # then do nothing.

    # Get the user name and password pair, or the session token.
    name = kwargs.get('name')
    password = kwargs.get('password')
    token = kwargs.get('token')

    # If the session token is a non-zero length string, then assume
    # that it is still a valid token and return that value.
    if type(token) == type(''):
        if len(token) > 0:
            return token

    # Call the "auth_session" function, passing the user name and
    # password.  If those arguments are invalid, then that function
    # handles that issue.
    token = auth_session(url, name = name, password = password)

    return token


def _is_doc(doc):

    if type(doc) != type({}):
        return False

    if '_id' not in doc:
        return False

    if type(doc['_id']) != type(''):
        return False

    if len(doc['_id']) == 0:
        return False

    if doc['_id'].startswith('_') == True:
        return False

    return True


def _is_docs(docs):

    if type(docs) != type({}):
        return False

    if 'docs' not in docs.keys():
        return False

    if type(docs['docs']) != type([]):
        return False

    return True


def _process_doc(orig, **kwargs):

    if '_id' not in orig.keys():
        return None

    if orig['_id'].startswith('_') == True:
        return None

    fields = kwargs.get('fields')

    if type(fields) == type([]):

        doc = {'_id': orig['_id']}

        for f in fields:
            if type(f) == type(''):
                if f in orig.keys():
                    doc[f] = orig[f]

    else:
        doc = orig

    # Was a callback passed to the query function?  If so, pass the
    # new document to the callback function.  Obviously good when
    # this module is use in an application.
    callback = kwargs.get('callback')

    if callable(callback) == True:

        try:

            doc = callback(doc)

            # The callback MUST NOT remove the "_id" field if the
            # calling application still expects the query function in
            # this module to return a list of documents.
            if '_id' not in doc.keys():
                doc = None

        except:
            doc = None

    return doc


def _process_results_row(ddict, row, **kwargs):

    if 'doc' not in row.keys():
        return None

    doc = _process_doc(row['doc'], **kwargs)

    if doc != None:
        ddict[doc['_id']] = doc

    return None


# #####################################################################
#
# PUBLIC FUNCTIONS.
#
# #####################################################################

def get_db(url):

    # Parse the database URL.
    p = urlparse(url)

    # Get the database name from the URL
    s = p.path.split('/')

    if len(s) > 1:
        db = s[1]

    else:
        return None

    return p.scheme + '://' + p.netloc + '/' + db


# #####################################################################
#
# PUBLIC FUNCTIONS ALSO EXPOSED ON THE COMMAND LINE.
#
# #####################################################################

def auth_session(url, **kwargs):

    # Parse the database URL.
    p = urlparse(url)

    # Get the database user name and password from the keyword
    # arguments.
    name = kwargs.get('name')
    password = kwargs.get('password')

    if type(name) != type('') or type(password) != type(''):
        return None

    try:

        r = requests.post(
            p.scheme + '://' + p.netloc + '/_session',
            headers = {'Content-Type': 'application/x-www-form-urlencoded'},
            data = {'name': name, 'password': password},
            timeout = 30)
        token = r.cookies.get('AuthSession')

    except:
        token = None

    return token


def backup(url, doc_ids, **kwargs):

    # Clean the URL so that it only contains the net location and the
    # database path.
    url = get_db(url)

    # Define the query to retrieve database documents.  Request the
    # attachments information because the attachments must be in the
    # backup.
    query = {'keys': doc_ids, 'include_docs': True, 'attachments': True}

    # Attempt to get a token.  If the token is None, proceed with the
    # assumption that basic authentication is being used.
    token = _get_token(url, **kwargs)

    # Call the "get" function to request database documents.
    orig_docs = get(url + '/_all_docs', [query], token = token)

    # This list will be populated by backup documents.
    bak_docs = []

    # Create the backup string to append to the document ID values for
    # the documents about to be backed up.
    #bak_dt = '.bak-' + datetime.now(UTC).strftime('%Y%m%d%H%M%S.%f')

    for orig_doc in orig_docs.get('docs', []):
        bak_docs.append(_gen_doc_bak(orig_doc))

    return write(url, {'docs': bak_docs}, num = NUM_DOCS, token = token)


def query(url, queries, **kwargs):

    # Clean the URL so that it only contains the net location and the
    # database path.
    tmp = get_db(url)

    # Attempt to get a view path from the URL string.  If an invalid
    # view path is given, the view will default to "_all_docs".
    view = None
    vspl = url.replace(tmp, '').split('/')

    try:

        if (vspl[1] == '_design' and vspl[3] == '_view') and len(vspl) >= 5:
            view = '/_design/' + vspl[2] + '/_view/' + vspl[4]

        if view == None and vspl[1] == '_all_docs':
            view = '/_all_docs'

    except:
        pass

    if view == None:
        raise ValueError('The given view path in the URL is invalid')

    # Iterate over the queries, making sure that some query keys are
    # set properly if raw data is not being requested.  Also, validate
    # the query key values in the case where the all_docs view is used.
    for q in queries['queries']:

        # The default action for this function is to make sure database
        # document are returned.  Make sure that the reduce flag is
        # set to false and the include_docs is set to True.  If we want
        # raw data, then make sure the raw argument is set to True.
        # Doing this will also allow for reductions to occur.
        if kwargs.get('raw') != True:

            q['reduce'] = False
            q['include_docs'] = True

            if 'fields' in q.keys():
                if type(q['fields']) == type([]):
                    if '_id' not in q['fields']:
                        q['fields'].append('_id')

        # Validate the key values in the queries.
        if view == '/_all_docs':

            if 'keys' in q.keys():

                if type(q['keys']) != type([]):
                    raise ValueError

                for k in q['keys']:
                    if type(k) != type(''):
                        raise ValueError

            for k in ('key', 'start_key', 'startkey', 'end_key', 'endkey',):
                if k in q.keys():
                    if type(q[k]) != type(''):
                        raise ValueError

    # Attempt to get a token.  If the token is None, assume that basic
    # authentication will be used.
    token = _get_token(tmp, **kwargs)

    # Create a dictionary of arguments to be passed to the post
    # functions of the requests package.
    params = {
        'headers': {'Content-Type': 'application/json'},
        'data': json.dumps({'queries': queries['queries']}),
        'timeout': 30}

    if token != None:

        params['headers']['X-CouchDB-WWW-Authenticate'] = 'Cookie'
        params['cookies'] = {'AuthSession': token}

    # Make the database request.
    try:
        results = requests.post(tmp + view + '/queries', **params).json()

    except Exception as e:
        results = {'error': 'python_exception', 'reason': str(e)}

    # Process the raw results from the database into a list of
    # documents, if raw data is not being requested.
    if kwargs.get('raw') != True and 'error' not in results.keys():

        # Count the iterations over the results so that we can get
        # the fields list from the corresponding query.
        qcount = 0

        # Populate this temporary dictionary with the documents from
        # the database response.  Using a dictionary to make sure
        # duplicates are not returned.
        ddict = {}

#        for result in results.get('results', []):
#            for row in result['rows']:
#                if 'doc' in row.keys():
#                    if row['doc']['_id'].startswith('_') == False:
#                        d[row['doc']['_id']] = row['doc']
        for result in results.get('results', []):

            for row in result['rows']:
                _process_results_row(
                    ddict,
                    row,
                    fields = queries['queries'][qcount].get('fields'),
                    callback = kwargs.get('callback'))

            qcount += 1

        # Get the ordered list of documents from the temporary
        # dictionary.
        l = sorted(
            list(ddict.values()),
            key = lambda x: x['_id'],
            reverse = True if kwargs.get('reverse') == True else False)

        results = {'docs': l}

    return results


def save(url, docs, **kwargs):

    # Clean the URL so that it only contains the net location and the
    # database path.
    url = get_db(url)

    # Store the incoming documents in a dictionary to ensure that there
    # are no duplicates.
    in_docs = _get_docs_dict(docs)

    # Attempt to get a token.  If the token is None, assume that basic
    # authentication will be used.
    token = _get_token(url, **kwargs)

    # Create a dictionary of arguments to be passed to the post
    # functions of the requests package.
    params = {
        'headers': {'Content-Type': 'application/json'},
        'timeout': 30}

    if token != None:

        params['headers']['X-CouchDB-WWW-Authenticate'] = 'Cookie'
        params['cookies'] = {'AuthSession': token}

    # Save responses from the write operations to this list.
    responses = []

    # Perform the write operations, one document at a time.
    for doc in in_docs.values():

        #responses.append(_save_doc(url, doc, token = token))
        # Attempt to save the document in the database.
        try:

            params['data'] = json.dumps(doc)
            response = requests.post(url, **params).json()

        except Exception as e:

            response = {'error': 'python_exception', 'reason': str(e)}
            sys.stderr.write(str(e))

        responses.append(response)

    return responses


def write(url, docs, num, **kwargs):

    # Clean the URL so that it only contains the net location and the
    # database path.
    url = get_db(url)

    # Get the list of valid documents to write to the database.
    docs_list = _get_docs_list(docs)

    # Get the number of input documents.  Then define counters that
    # will be used to slice the documents list.
    num_docs = len(docs_list)
    start = 0
    end = num

    if end > num_docs:
        end = num_docs

    # Attempt to get a token.
    token = _get_token(url, **kwargs)

    # Modify the URL with the "_bulk_docs" endpoint to perform the bulk
    # write operations.
    url += '/_bulk_docs'

    # Build the parameters to pass to the post function of the requests
    # package.
    params = {'headers': {'Content-Type': 'application/json'}, 'timeout': 30}

    if token != None:

        params['headers']['X-CouchDB-WWW-Authenticate'] = 'Cookie'
        params['cookies'] = {'AuthSession': token}

    # Iterate over slices of the list of documents, writing those
    # documents in bulk into the database.
    responses = []

    while start < num_docs:

        sliced = {'docs': docs_list[start:end]}

        if len(sliced['docs']) > 0:

            # Update the params with the current data.
            params['data'] = json.dumps(sliced)

            try:
                responses.extend(requests.post(url, **params).json())

            except Exception as e:
                sys.stderr.write(str(e))

        # Increment the start and end indices for slicing the
        # docs_list.
        if (end + num) > num_docs:

            start += num
            end = num_docs

        else:

            start += num
            end += num

    return responses


# #####################################################################
#
# RUN THIS MODULE FROM THE COMMAND LINE.
#
# #####################################################################

def main():

    # Command line parser.
    parser = argparse.ArgumentParser()

    # Define the base parser arguments.
    parser.add_argument(
        'url',
        type = str,
        help = 'The CouchDB database address.  Basic authentication '\
            + 'credentials will supercede a user name, passowrd or token '\
            + 'authentication')
    parser.add_argument(
        '--name',
        type = str,
        help = 'Username for database authentication')
    parser.add_argument(
        '--password',
        type = str,
        help = 'User password for database authentication')
    parser.add_argument(
        '--token',
        type = str,
        help = 'A CouchDB session token for database authentication')
    parser.add_argument(
        '-o',
        '--outfile',
        type = argparse.FileType('w'),
        default = sys.stdout)

    # Define a subparser.
    sp = parser.add_subparsers(dest = 'cmd')

    # Define the document backup argument.
    parser_bak = sp.add_parser('backup')
    parser_bak.add_argument(
        'doc_ids',
        nargs = '*',
        default = [],
        help = 'Backup documents with the given document ID values.')

    # Define the document retrieval argument.
    parser_query = sp.add_parser('query')
    parser_query.add_argument(
        '--infile',
        type = argparse.FileType('r'),
        default = (None if sys.stdin.isatty() else sys.stdin),
        help = 'A file containing CouchDB view queries as JSON')
    parser_query.add_argument(
        '--raw',
        action = 'store_true',
        help = 'Return the raw database results')

    # Define the save arguments.
    parser_save = sp.add_parser('save')
    parser_save.add_argument(
        '-i',
        '--infile',
        type = argparse.FileType('r'),
        default = (None if sys.stdin.isatty() else sys.stdin))

    # Define the bulk write arguments.
    parser_write = sp.add_parser('write')
    parser_write.add_argument(
        '-i',
        '--infile',
        type = argparse.FileType('r'),
        default = (None if sys.stdin.isatty() else sys.stdin))
    parser_write.add_argument(
        '-n',
        type = int,
        default = NUM_DOCS,
        help = 'Bulk writes are sliced up by this number of documents')

    # Parse the command line arguments.
    args = parser.parse_args()

    # Load any input data from the command line.
    in_data = None

    if args.cmd in ('save', 'write',):

        # Attempt to load the JSON input data.
        try:
            in_data = json.loads(args.infile.read())

        except Exception as e:
            in_data = None

    # Run the script based on the sub-command given.
    if args.cmd == 'backup':
        output = backup(
            args.url,
            args.doc_ids,
            name = args.name,
            password = args.password,
            token = args.token)

    elif args.cmd == 'query':

        try:

            queries = json.loads(args.infile.read())

            output = query(
                args.url,
                queries,
                raw = args.raw,
                name = args.name,
                password = args.password,
                token = args.token)

        except Exception as e:
            output = {'error': 'python_exception', 'reason': str(e)}

    elif args.cmd == 'save' and in_data != None:
        output = save(
            args.url,
            in_data,
            name = args.name,
            password = args.password,
            token = args.token)

    elif args.cmd == 'write' and in_data != None:
        output = write(
            args.url,
            in_data,
            args.n,
            name = args.name,
            password = args.password,
            token = args.token)

    else:
        output = {'token': auth_session(
            args.url, name = args.name, password = args.password)}

    # Write the output of the commands to a file or to standard out.
    try:
        if output != None:
            args.outfile.write(json.dumps(output) + '\n')

    except Exception as e:
        sys.stderr.write(str(e) + '\n')

    return None


if __name__ == '__main__':

    # Run from command line.
    main()

    # Get out of here!
    sys.exit(0)
