# Standard library imports.
import os

# Setuptools package imports.
from setuptools import setup

# Read the README.rst file for the 'long_description' argument given
# to the setup function.
README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# Allow setup.py to be run from any path.
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))


setup(
    name = 'Swoodles',
    version = '1a2',
    py_modules = ['swoodles'],
    entry_points = {'console_scripts': ['swoodles = swoodles:main']},
    install_requires = ['requests'],
    license = 'BSD License',
    description = 'A write utility for CouchDB',
    long_description = README,
    url = 'https://bitbucket.org/notequal/swoodles/',
    author = 'Stanley Engle',
    classifiers = [
        #'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',
        'Topic :: Database'],)
